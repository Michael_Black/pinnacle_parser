from pinnacle.apiclient import APIClient
import os
import configparser
# -*- coding: utf-16 -*-


def showAllParametrs():
    None


def showSportParametrs(sport):
    None


def inputN(p):
    n = input('Выберите пункт меню: ')
    if n.isdigit():
        if int(n) == float(n):
            if 0 <= int(n) <= p:
                return int(n)
    return -1


def currentParametrs(api):
    n = -1
    while n == -1:
        os.system('cls')
        print("Параметры сбора данных")
        print('----------------------------------')
        print('1. Показать все виды спорта и лиги')
        print('2. Показать лиги выбранного вида спорта')
        print('0. Вернуться')
        print('----------------------------------')
        n = inputN(2)
        if n == 1:
            showAllParametrs()
            os.system('cls')
        if n == 2:
            showSportParametrs(0)
            os.system('cls')
        if n == 0:
            main_menu(api)
            os.system('cls')


def addLeague(api: APIClient):
    #получение id спорта и лиги для парсинга
    sport_id, sport_name = selectSportsSite(api)
    league_id, league_name = selectLeagueSite(api, sport_id, sport_name)

    # создание класса конфигураций
    conf = configparser.RawConfigParser()

    # чтение файла конфигураций
    conf.read("parser parametrs.conf")

    # если нет секции для выбранного спорта - создать секцию
    if not conf.has_section(sport_name):
        conf.add_section(sport_name)
<<<<<<< Updated upstream

    # сохранение параметра id спорта
    conf.set(sport_name, 'id', sport_id)

    # если нет списка с id лиг для парса - создать пустой список, иначе добавить элемент
    if not conf.has_option(sport_name, 'leagues_id'):
        conf.set(sport_name, 'leagues_id', '')
        lid = str(league_id)
    else:
        lid = conf.get(sport_name, 'leagues_id')
        lid += ','+str(league_id)
    conf.set(sport_name, 'leagues_id', lid)

    if not conf.has_option(sport_name, 'leagues_name'):
        conf.set(sport_name, 'leagues_name', '')
        lid = str(league_name)
    else:
        lid = conf.get(sport_name, 'leagues_name')
        lid += ','+str(league_name)
    conf.set(sport_name, 'leagues_name', lid)
=======
    # сохранение параметра id спорта
    conf.set(sport_name, 'id', sport_id)

    if not conf.has_option(sport_name, 'leagues_name'):
        conf.set(sport_name, 'leagues_name', league_name)
    # если нет списка с id лиг для парса - создать пустой список, иначе добавить элемент
    if not conf.has_option(sport_name, 'leagues_id'):
        conf.set(sport_name, 'leagues_id', league_id)
        os.system('cls')
        print('Данные были внесены: Спорт: ', sport_name, ' Лига: ', league_name)
        input()
    else:
        lid = conf.get(sport_name, 'leagues_id')
        list_id = lid.split(',')
        ok = 0
        for ids in list_id:
            if int(ids) == league_id:
                ok = 1
                os.system('cls')
                print('Выбранная лига уже занесена в список для сбора данных')
                input()
                break
        if ok == 0: # если выбранного id лиги нет в файле для парсинга то добавить его и имя лиги
            lid = conf.get(sport_name, 'leagues_id')
            lid += ','+str(league_id)
            conf.set(sport_name, 'leagues_id', lid)

            if not conf.has_option(sport_name, 'leagues_name'):
                conf.set(sport_name, 'leagues_name', league_name)
            else:
                lid = conf.get(sport_name, 'leagues_name')
                lid += ','+str(league_name)
                conf.set(sport_name, 'leagues_name', lid)

            os.system('cls')
            print('Данные были внесены: Спорт: ', sport_name, ' Лига: ', league_name)
            input()
>>>>>>> Stashed changes

    with open("parser parametrs.conf", "w") as config:
        conf.write(config)




<<<<<<< Updated upstream

=======
>>>>>>> Stashed changes
def selectLeagueSite(api: APIClient, sport_id, sport_name):
    os.system('cls')
    print('Лиги ', sport_name)
    print('------------')

    leagues = api.reference_data.get_leagues(sport_id)
    i = 0
    half = len(leagues) // 2
    ost  = len(leagues) % 2
    while i < half:
        t1 = str(i + 1) + ' ' + leagues[i]['name']
<<<<<<< Updated upstream
        t1 += (55 - len(t1)) * ' ' + '|' + ' '
        t2 = str(i + half + ost + 1) + ' ' + leagues[i + half + ost]['name']
        print(t1 + t2)
        i += 1
    if len(leagues) % 2 == 1:
        t1 = str(half + 1) + ' ' + leagues[half]['name']
        t1 += (55 - len(t1)) * ' ' + '|' + ' '
=======
        t1 += (65 - len(t1)) * ' ' + '|' + ' '
        t2 = str(i + half + ost + 1) + ' ' + leagues[i + half + ost]['name']
        tt =  t1.encode('utf-8') + t2.encode('utf-8')
        print(tt)
        i += 1
    if len(leagues) % 2 == 1:
        t1 = str(half + 1) + ' ' + leagues[half]['name']
        t1 += (65 - len(t1)) * ' ' + '|' + ' '
>>>>>>> Stashed changes
        print(t1)
    print('0  Вернуться')
    print('------------')
    print(len(leagues))
    n = inputN(len(leagues))
    if 0 < n <= len(leagues):
        return leagues[n-1]['id'], leagues[n-1]['name']
    if n == 0:
        addLeague(api)


def selectSportsSite(api: APIClient):
    print('Виды спорта')
    print('------------')
    sports = api.reference_data.get_sports()
    i = 0
    while i < len(sports):
        # подсчет пробелов для выравнивания
        if sports[i]['id'] < 10:
            temp = 2
        else:
            temp = 1

        print(str(i+1), (temp + 1) * ' ', sports[i]['name'])
        i += 1
    print('0  Вернуться')
    print('------------')
    n = inputN(sports[len(sports)-1]['id'])
    if 0 < n <= sports[len(sports)-1]['id']:
        id = sports[n-1]['id']
        name = sports[n-1]['name']
        return id, name
    if n == 0:
        currentParametrs(api)


def removeLeague():
    None


# функция вызова основоного меню настройщика парсера
def main_menu(api: APIClient):
    n = -1
    while n == -1:
        os.system('cls')
        print("Pinnacle Parser")
        print('----------------------------------')
        print('1. Показать параметры сбора данных')
        print('2. Добавить лигу в сбор данных')
        print('3. Удалить лигу из сбора данных')
        print('----------------------------------')
        n = inputN(3)
        if n == 1:
            currentParametrs(api)
            os.system('cls')
        if n == 2:
            addLeague(api)
            os.system('cls')
        if n == 3:
            removeLeague()
            os.system('cls')
